import React, {Component} from 'react';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import Card from 'react-bootstrap/Card'


export default class DetailsCharacter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            image: '',
            sexo: '',
            slug: '',
            rank: null,
            casa: '',
            libros: null,
            titulos: null
        }
    }

    async componentWillReceiveProps(nextProps) {
        if(nextProps.match.params.id == this.props.match.params.id){
          return;
        } else {
            const data = await fetch('http://localhost:3000/characters/' + nextProps.match.params.id)
            .then(result => {
               return result.json();
            }).then(data => {
                if(data.result){
                  const character = data.result
                  if(character) {
                    this.setState({
                      nombre:  character.name,
                      image: character.image,
                      sexo:  character.sex,
                      slug:  character.slug,
                      rank:  character.rank,
                      casa:  character.house,
                      libros: character.books,
                      titulos: character.titles
                    })
                  }
                }
            })
        }
    }

    async componentDidMount() {
        let id = this.props.match.params.id;
        const data = await fetch('http://localhost:3000/characters/' + id)
        .then(result => {
           return result.json();
        }).then(data => {
            if(data.result){
              const character = data.result
              if(character) {
                this.setState({
                  nombre:  character.name,
                  image: character.image,
                  sexo:  character.sex,
                  slug:  character.slug,
                  rank:  character.rank,
                  casa:  character.house,
                  libros: character.books,
                  titulos: character.titles
                })
              }
            }
        })
    }

 

    render() {
        return (
            <>
            <Card className="text-center" style={{ width: '22rem', height: '32rem' }}>
                <Card.Header>{this.state.nombre}</Card.Header>
                <Card.Img variant="top" src={this.state.image} />
                <Card.Body>
                    <Card.Title>{this.state.casa}</Card.Title>
                    <Card.Title>{this.state.sexo}</Card.Title>
                    <Card.Text>
                        {this.state.rank} 
                        {this.state.libros}
                    </Card.Text>
                </Card.Body>
                <Card.Footer className="text-muted">{this.state.titulos}</Card.Footer>
            </Card>
            </>     
        )
    }
}
