import React, {Component} from 'react';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { BrowserRouter,Link,Route,Switch } from 'react-router-dom';

/**
 * IMPORT COMPONENT
 */

import DetailsCharacters from './details'

export default class ListCharacters extends Component {
    constructor(props) {
        super(props);
        this.state = {
            characters: []
        }
    }

    componentDidMount() {
        let initialCharacter = [];
        fetch('http://localhost:3000/characters')
            .then(results => {
                return results.json();
            }).then(data => {
            initialCharacter = data.result.map((character) => {
                return character
            });
            this.setState({
                characters: initialCharacter,
            })
        });
    }

    imgFormatter(cell,row) {
        return (
            <Link  to={`/details/${row._id}`}>Details</Link>
        )
    }

    render() {
        return (
            <>
                <BootstrapTable data={this.state.characters} pagination striped hover>
                    <TableHeaderColumn dataField='name' isKey filter={{ type: 'TextFilter', delay: 100 }}>Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='slug'>Slug</TableHeaderColumn>
                    <TableHeaderColumn dataField='gender'>Gender</TableHeaderColumn>
                    <TableHeaderColumn dataField='house' filter={{ type: 'TextFilter', delay: 100 }}>House</TableHeaderColumn>
                    <TableHeaderColumn dataField='alive'>Alive</TableHeaderColumn>
                    <TableHeaderColumn dataField='edit' dataFormat={ this.imgFormatter }>Details</TableHeaderColumn>
                </BootstrapTable>,
             
            </>     
        )
    }
}
