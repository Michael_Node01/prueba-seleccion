import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import {Form, Col, Row} from "react-bootstrap";

// ADD COMPONENT
import ListCharacters from './components/characters/list';
import DetailsCharacters from './components/characters/details'

ReactDOM.render(
    <BrowserRouter>
         <Form.Row>
            <Col md={9} lg={9}> 
                <ListCharacters /> 
            </Col> 
            <Col md={3} lg={3}> 
                <Switch>
                    <Route exact path="/details/:id" component={DetailsCharacters}/>
                </Switch>
            </Col> 
         </Form.Row>
    </BrowserRouter>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
