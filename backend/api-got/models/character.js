'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CharacterSchema = Schema({
    titles: [String],
    spouse: [String],
    children: [String],
    allegiance: [String],
    books:[String],
    plod: Number,
    longevity: [Number],
    plodB: Number,
    plodC: Number,
    longevityB: [Number],
    longevityC: [Number],
    name: {
        type:String,
        index: true
    },
    slug: String,
    gender: String,
    culture: String,
    image: String,
    house: String,
    alive: Boolean,
    pagerank: [{
        title:String,
        rank: Number
    }],
    age:Number
});


CharacterSchema.statics.list = async function  (filters,  pages, perPage) {
    const query =  Character.find(filters);
    if (pages !== undefined && perPage !== undefined) {
        query.skip((perPage * pages) - perPage);
        query.limit(perPage);
      }
    const CharacterArr = await query.exec();
    return CharacterArr;
}

const Character = mongoose.model('characters', CharacterSchema);

module.exports = Character;

