'use strict';

/** 
 * Dependencies 
 */
 const namedRoutes = require('../lib/namedRoutes');
 const characterRouter = require('./character');
 // Router class 
 class Router {
     constructor(app) {
        app.use(namedRoutes.character, characterRouter)
        
     }
 }

 module.exports = (params) => new Router(params);