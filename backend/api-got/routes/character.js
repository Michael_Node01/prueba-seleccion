'use strict';

const express = require('express');
const router = express.Router();
const Character =    require('../models/character');
var createError = require('http-errors');

/**
 * GET ALL CHARACTER 
 */

router.get('/', async function (req, res, next) {

    try {
      
        const page = parseInt(req.query.page) || 1;
        const recordsPerPage = 100;
        const filters = {};

        const character = await Character.list(filters,  page, recordsPerPage);
        if( character ){
            res.json({result: character})
        }
    
    } catch (err) {
        next(err)
    }
});


router.get('/:id', async (req, res, next) => {
    console.log('details',req.params.id)
	try {
		const characterId =req.params.id;
		const character = await Character
			.findById({_id: characterId })
			.exec();
		if(!character){
			next(createError(404));
			return;
        }
       
		res.json({result: character})
	} catch (err) {
		next(err);
	}
});

module.exports = router;