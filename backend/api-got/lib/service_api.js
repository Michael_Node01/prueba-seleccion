'use strict';

const https = require('https');
const db = require('./mongoose');
const Character = require('../models/character');

db.once("open", async () => {
    try {
        await removeBD();
        await saveCharacters();
    } catch (err) {
        console.log("Error", err);
        process.exit(1);
    }
});

async function removeBD() {
    await Character.remove(function(err) {
        if (err) {
            console.log("hubo un error", err);
            return;
        }
        console.log('Se borraron los Character de la BD')
    });
}

async function saveCharacters(response) {
    https.get('https://api.got.show/api/general/characters', (resp) => {
        let data = '';

        resp.on('data', (chunk) => {
          data += chunk;
        });
      
        resp.on('end', async () => {
          var { book } = JSON.parse(data);
          await Character.insertMany(book);
          process.exit(0);
        });
      
        }).on("error", (err) => {
            console.log("Error: " + err.message);
        });
}

