'use strict';

const mongoose = require('mongoose');
const conn = mongoose.connection;

conn.on('error', err => {
    console.error('error de conexión', err);
    process.exit(1);
});

conn.once('open', () => {
    console.log('Conectado a MongoDB en', conn.name);
})

mongoose.connect(
    'mongodb://localhost:27017/gofc', { 
        useNewUrlParser: true , 
        useCreateIndex: true 
    });

module.exports = conn;
